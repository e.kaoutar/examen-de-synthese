﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Systeme_Consultation
{
    public class Fournisseur
    {
        public string Name { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public string Email  { get; set; }
        public string Logo  { get; set; }
    }
}