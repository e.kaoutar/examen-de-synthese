﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Systeme_Consultation
{
    public partial class updateData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"].ToString();
            string name = Request.QueryString["name"].ToString();
            string adress = Request.QueryString["adress"].ToString();
            string phone = Request.QueryString["phone"].ToString();
            string email = Request.QueryString["email"].ToString();
            string logo = Request.QueryString["logo"].ToString();


            DataGateway data = new DataGateway();

            data.openConnection();

            bool statut = false;

            int Id = Convert.ToInt32(id); 

            statut = data.updateData(Id, name, adress, phone, logo, email);

            if (statut)
            {
                Response.Write("OK");
            }
            else
            {
                Response.Write("ERROR");
            }
        }
    }
}