﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Systeme_Consultation
{
    public class DataGateway
    {

        public SqlConnection myCon;
        private SqlCommand command;
        private SqlDataReader reader;

        //initialize object
        Person per = new Person
        {
            Username = "",
            Password = "",
            Type = ""
        };


        Fournisseur four = new Fournisseur
        {
            Name="",
            Adress="",
            Phone="",
            Email="",
            Logo=""
        };



        public void openConnection()
        {
            myCon = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Kaoutar\Desktop\Systeme_Consultation\Systeme_Consultation\App_Data\consultationdb.mdf;Integrated Security=True");

            myCon.Open();

        }

        public Person loginUser(String username, String password)
        {
            try
            {
                command = new SqlCommand("select * from Person where Username=@usr and Password=@pw;", myCon);
                command.Parameters.AddWithValue("usr", username);
                command.Parameters.AddWithValue("pw", password);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    per = new Person
                    {
                        Username = reader["Username"].ToString(),
                        Password = reader["Password"].ToString(),
                        Type = reader["Type"].ToString()
                    };
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            reader.Close();
            return per;
        }

        public string getFournisseurs()
        {
            string infosfour = "";
            try
            {
                command = new SqlCommand("select * from Fournisseur order by Name;", myCon);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    infosfour += reader["Logo"].ToString() + ";";
                    infosfour += reader["Name"].ToString() + ";*";
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return infosfour;
        }

        public Fournisseur getFournisseurByName(string name)
        {
            try
            {
                command = new SqlCommand("select * from Fournisseur where Name=@nom;", myCon);
                command.Parameters.AddWithValue("nom", name);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    four = new Fournisseur
                    {
                        Name = reader["Name"].ToString(),
                        Adress = reader["Adress"].ToString(),
                        Email = reader["Email"].ToString(),
                        Phone = reader["Phone"].ToString(),
                        Logo = reader["Logo"].ToString()
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return four;
        }


        public bool insertData(String name, String adress, String phone, String logo, String email)
        {
            int result = 0;
            try
            {
                command = new SqlCommand("insert into Fournisseur(Name, Adress, Phone, Email, Logo) values (@name, @adress, @phone, @logo, @email);", myCon);
                command.Parameters.AddWithValue("name", name);
                command.Parameters.AddWithValue("adress", adress);
                command.Parameters.AddWithValue("phone", phone);
                command.Parameters.AddWithValue("logo", logo);
                command.Parameters.AddWithValue("email", email);
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }

        public bool updateData(int id, String name, String adress, String phone, String logo, String email)
        {
            int result = 0;
            try
            {
                command = new SqlCommand("update Fournisseur set Name=@name, Adress=@adress, Phone=@phone, Email=@email, Logo=@logo where Id=@id;", myCon);
                command.Parameters.AddWithValue("id", id);
                command.Parameters.AddWithValue("name", name);
                command.Parameters.AddWithValue("adress", adress);
                command.Parameters.AddWithValue("phone", phone);
                command.Parameters.AddWithValue("logo", logo);
                command.Parameters.AddWithValue("email", email);
                result = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }

        public void closeConnection()
        {
            myCon.Close();
        }


    }
}