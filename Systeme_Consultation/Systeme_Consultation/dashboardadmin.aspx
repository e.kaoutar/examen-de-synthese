﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.Master" AutoEventWireup="true" CodeBehind="dashboardadmin.aspx.cs" Inherits="Systeme_Consultation.dashboardadmin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="lib/jquery/jquery.min.js"></script>
    <script src="Scripts/adminfunctions.js"></script>
    <link href="Styles/stylelanding.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <nav class="navbar navbar-light bg-light">
        <h3 class="navbar-brand">Administrateur</h3>
           <a href="loginPage.aspx" class="btn btn-dark">Se déconnecter</a>
    </nav>
    <button type="button" class="btn btn-outline-secondary" id="btnAjoutFour">Ajouter un fournisseur</button>
    <button type="button" class="btn btn-outline-secondary" id="btnModifierFour">Modifier un fournisseur</button>

    <div id="adminboard">

    </div>

 
</asp:Content>
