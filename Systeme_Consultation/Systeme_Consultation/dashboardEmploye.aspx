﻿<%@ Page Title="" Language="C#" MasterPageFile="~/employe.Master" AutoEventWireup="true" CodeBehind="dashboardEmploye.aspx.cs" Inherits="Systeme_Consultation.dashboardEmploye" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="lib/jquery/jquery.min.js"></script>
    <script src="Scripts/getFournisseurs.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <nav class="navbar navbar-light bg-light">
            <h3 class="navbar-brand">Employé</h3>
               <a href="loginPage.aspx" class="btn btn-dark">Se déconnecter</a>
        </nav>

    <div class="alert alert-info" role="alert">
        Liste des fournisseurs    
    </div>
    <div id="listfournisseurs">

    </div>

</asp:Content>
