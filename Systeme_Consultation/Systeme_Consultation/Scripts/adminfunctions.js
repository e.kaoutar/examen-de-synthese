﻿$(document).ready(function () {
    $("#btnAjoutFour").click(function () {
        //alert("Ajout"); --OK

        document.getElementById("adminboard").innerHTML="";
        document.getElementById("adminboard").innerHTML += "<form>";
        document.getElementById("adminboard").innerHTML += "<div class='col'><input type='text' class='form-control' placeholder='Nom fournisseur' style='width:50%;' id='nomfour'></div><br/>";
        document.getElementById("adminboard").innerHTML += "<div class='col'><input type='text' class='form-control' placeholder='Adresse fournisseur' id='adressefour' style='width:50%;'></div><br/>";
        document.getElementById("adminboard").innerHTML += "<div class='col'><input type='text' class='form-control' placeholder='Télephone fournisseur' id='phonefour' style='width:50%;'></div><br/>";
        document.getElementById("adminboard").innerHTML += "<div class='col'><input type='text' class='form-control' placeholder='Lien vers l'image' id='lienimage' style='width:50%;'></div><br/>";
        document.getElementById("adminboard").innerHTML += "<div class='col'><input type='text' class='form-control' placeholder='Email fournisseur' id='emailfour' style='width:50%;'></div><br/>";
        document.getElementById("adminboard").innerHTML += "<button type='submit' class='btn btn-primary' id='btnEnregistrer' style='width:30%; margin-left: 9%;'>Enregistrer</button>";
        document.getElementById("adminboard").innerHTML += "</form>";

        $("#btnEnregistrer").click(function () {
            var nom = $("#nomfour").val();
            var adresse = $("#adressefour").val();
            var phone = $("#phonefour").val();
            var logo = $("#lienimage").val();
            var email = $("#emailfour").val();

            var req = new XMLHttpRequest();
            req.open("GET", "insertData.aspx?name=" + nom + "&adress=" + adresse + "&phone=" + phone + "&logo=" + logo + "&email=" + email, false);
            req.send(null);

            //alert(nom);

            if (req.responseText == "OK") {
                alert("Bien ajouté");
            }
            else {
                alert("Echec");
            }

        });
        
       
    });

    $("#btnModifierFour").click(function () {
           //alert("Modif");  --OK

        document.getElementById("adminboard").innerHTML = "";
        document.getElementById("adminboard").innerHTML += "<form>";
        document.getElementById("adminboard").innerHTML += "<div class='col'><input type='text' class='form-control' placeholder='IDENTIFIANT' style='width:50%;' id='idfour'></div><br/>";
        document.getElementById("adminboard").innerHTML += "<div class='col'><input type='text' class='form-control' placeholder='Nouveau Nom fournisseur' style='width:60%;' id='nomfour'></div><br/>";
        document.getElementById("adminboard").innerHTML += "<div class='col'><input type='text' class='form-control' placeholder='Nouvelle Adresse fournisseur' id='adressefour' style='width:60%;'></div><br/>";
        document.getElementById("adminboard").innerHTML += "<div class='col'><input type='text' class='form-control' placeholder='Nouveau Télephone fournisseur' id='phonefour' style='width:60%;'></div><br/>";
        document.getElementById("adminboard").innerHTML += "<div class='col'><input type='text' class='form-control' placeholder='Lien vers l'image' id='lienimage' style='width:60%;'></div><br/>";
        document.getElementById("adminboard").innerHTML += "<div class='col'><input type='text' class='form-control' placeholder='Nouveau Email fournisseur' id='emailfour' style='width:60%;'></div><br/>";
        document.getElementById("adminboard").innerHTML += "<button type='submit' class='btn btn-primary' id='btnEnregistrer' style='width:30%; margin-left: 9%;'>Enregistrer</button>";
        document.getElementById("adminboard").innerHTML += "</form>";

        $("#btnEnregistrer").click(function () {
            var ID = $("#idfour").val();
            var nom = $("#nomfour").val();
            var adresse = $("#adressefour").val();
            var phone = $("#phonefour").val();
            var logo = $("#lienimage").val();
            var email = $("#emailfour").val();

            var req = new XMLHttpRequest();
            req.open("GET", "updateData.aspx?id=" + ID +"&name=" + nom + "&adress=" + adresse + "&phone=" + phone + "&logo=" + logo + "&email=" + email, false);
            req.send(null);

            //alert(nom);

            if (req.responseText == "OK") {
                alert("Bien modifié");
            }
            else {
                alert("Echec");
            }

        });


     

    });




});