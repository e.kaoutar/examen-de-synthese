﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Systeme_Consultation
{
    public partial class traitementDetailsFour : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string namefour = Request.QueryString["namefour"].ToString();

            //sauvegarder le nom du fournisseur seulement

            DataGateway data = new DataGateway();

            //ouverture connexion
            data.openConnection();

            Fournisseur F;

            F = data.getFournisseurByName(namefour);

            //stocker les donnees dans des sessions
            Session["fnom"] = F.Name;
            Session["fadress"] = F.Adress;
            Session["fphone"] = F.Phone;
            Session["femail"] = F.Email;
            Session["flogo"] = F.Logo;

            data.closeConnection();

        }
    }
}