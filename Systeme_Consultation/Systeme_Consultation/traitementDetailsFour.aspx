﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="traitementDetailsFour.aspx.cs" Inherits="Systeme_Consultation.traitementDetailsFour" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%= Session["fnom"].ToString() %></title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
    <form id="form1" runat="server">
       <div class="jumbotron">
           <img src="Images/<%= Session["flogo"] %>" alt="introuvable" />
          <hr class="my-4"/>
          <p><b>Nom du fournisseur:</b> <%= Session["fnom"].ToString() %></p>
          <p><b>Adresse:</b> <%= Session["fadress"].ToString() %></p>
          <p><b>Télephone:</b> <%= Session["fphone"].ToString() %></p>
          <p><b>Adresse électonique:</b> <%= Session["femail"].ToString() %></p>
          <a class="btn btn-warning btn-lg" href="dashboardEmploye.aspx" role="button"> << Retour au catalogue</a>
    </div>
    </form>

             <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


</body>
</html>

