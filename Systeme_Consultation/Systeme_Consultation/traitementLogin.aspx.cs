﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Systeme_Consultation
{
    public partial class traitementLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string username = Request.QueryString["username"].ToString();
            string password = Request.QueryString["password"].ToString();
            string verification = "";


            DataGateway data = new DataGateway();

            //ouverture connexion
            data.openConnection();

            //initialiser la personne
            Person P;


            //LOGIN FUNCTION
            P = data.loginUser(username, password);

            if(P.Username==username && P.Password==password && P.Type == "A")
            {
                verification = "adminpage";
                Session["ausername"] = username;
                Session["apassword"] = password;

            } else if(P.Username == username && P.Password == password && P.Type == "E")
            {
                verification = "employepage";
                Session["eusername"] = username;
                Session["epassword"] = password;
            }
            else
            {
                verification = "nondefined";
            }

            Response.Write(verification);

        }

    }
}